#!/bin/sh
#
# Copyright (c) 2022 Sean Davies <skd@skd.id.au>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

outdir=/etc/nginx/conf.d

die()
{
	echo "$1" 1>&2
	exit 1
}

usage()
{
	die "usage: ${0##*/} [-a] [-r | -x] cctld"
}

action=deny
rflag=0
xflag=0
while getopts arx arg; do
	case ${arg} in
	a)	action=allow ;;
	r)	rflag=1 ;;
	x)	xflag=1 ;;
	*)	usage ;;
	esac
done
shift $((OPTIND - 1))
[ "$#" -eq 1 ] || usage

if [ "${rflag}" -eq 1 ] && [ "${xflag}" -eq 1 ]; then
	usage
fi

case $1 in
[A-Z][A-Z]) cctld=$(echo "$1" | tr '[:upper:]' '[:lower:]') ;;
[a-z][a-z]) cctld=$1;;
*) usage ;;
esac
url=http://www.ipdeny.com/ipblocks/data/aggregated/${cctld}-aggregated.zone

if ! command -v curl >/dev/null; then
	die "${0##*/}: curl: command not found"
fi

if [ "${rflag}" -eq 1 ] && ! command -v nginx >/dev/null; then
	die "${0##*/}: nginx: command not found"
fi

if [ "${xflag}" -eq 0 ] && [ "$(id -u)" -ne 0 ]; then
	die "${0##*/}: needs root privileges"
fi

response=$(curl -Ss -o - -w "%{http_code}" "${url}")
http_status=$(echo "${response}" | tail -1)
case ${http_status} in
200)	;;
000)	exit 1 ;;
*)	die "${0##*/}: curl: ${http_status}" ;;
esac

output=$(echo "${response}" | sed '$d' | \
	while read -r line; do echo "${action} ${line};"; done)
[ -n "${output}" ] || exit 0

if [ "${xflag}" -eq 0 ]; then
	outfile=${outdir}/${url##*/}

	if [ ! -f "${outfile}" ] || [ "${output}" != "$(cat "${outfile}")" ]; then
		if [ ! -d "${outdir}" ]; then
			mkdir -p "${outdir}"
		fi

		echo "${output}" >"${outfile}"
		if [ "${rflag}" -eq 1 ]; then
			nginx -tq
			systemctl reload nginx
		fi
	fi
else
	echo "${output}"
fi
