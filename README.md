NGINX-GEOBLOCK(8) - System Manager's Manual

# NAME

**nginx-geoblock** - restrict nginx connections by geographical location

# SYNOPSIS

**nginx-geoblock**
\[**-a**]
\[**-r**&nbsp;|&nbsp;**-x**]
*cctld*

# DESCRIPTION

**nginx-geoblock**
is a utility that allows you restrict
nginx(8)
connections by geographical location by downloading and processing the country
IP blocks for the country code top-level domain given as
*cctld*.

The IP addresses are prepped using the default filter action to
*deny*
the connections, the file must then be soured using an include statement within
the
nginx(8)
configuration file.

The output file is saved to the following location where
*xx*
is the two letter
*cctld*
identifier.

*/etc/nginx/conf.d/xx-aggregated.zone*

Alternatively, the processed data can printed to stdout.

The options are as follows:

**-a**

> Change the filter action from
> *deny*
> to
> *allow*.

**-r**

> Test and reload the
> nginx(8)
> service.

**-x**

> Print processed data to stdout and exit.

# EXIT STATUS

The **nginx-geoblock** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# EXAMPLES

The following creates a deny list of all New Zealand IP addresses.

	# nginx-geoblock nz

The file must then be sourced within the
nginx(8)
configuration.

	server {
	    include /etc/nginx/conf.d/nz-aggregated.zone;
	    ...
	}

The following creates an allow list of only Australian IP addresses.

	# nginx-geoblock -a au

And sourced within
nginx(8)
with an additional deny statement.

	server {
	    include /etc/nginx/conf.d/au-aggregated.zone;
	    deny all;
	    ...
	}

# SEE ALSO

curl(1),
nginx(8)

[https://www.ipdeny.com](https://www.ipdeny.com)

# AUTHORS

Sean Davies &lt;[skd@skd.id.au](mailto:skd@skd.id.au)&gt;

# CAVEATS

Depends on
curl(1).
